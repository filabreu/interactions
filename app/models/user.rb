class User < ApplicationRecord
  has_many :events, foreign_key: :user_id
  has_many :received_events, class_name: 'Event', foreign_key: :recipient_id

  validates :source_id, presence: true
  validates :source_id, uniqueness: true

  def self.find_or_create_from_github(data)
    return self.find_by(source_id: data['id']) if self.exists?(source_id: data['id'])

    self.create(
      source_id: data['id'],
      username: data['login'],
      avatar_url: data['avatar_url'],
      profile_url: data['html_url']
    )
  end

  def interactions(date_since: nil)
    @interactions ||= if date_since
      events.where("source_created_at >= ?", date_since) +
      received_events.where("source_created_at >= ?", date_since)
    else
      events + received_events
    end
  end

  def ranked_interaction_types(date_since: nil)
    @ranked_interaction_types ||=
      interactions(date_since: date_since).reduce({}) do |hash, i|
        hash[i.type] = hash[i.type].to_i + 1
        hash
      end.map do |k, v|
        { type: k, count: v }
      end
  end

  def interacted_users
    @interacted_users ||= User.where(id: ordered_interacted_users_ids)
  end

  def ordered_interacted_users_ids
    @ordered_interacted_users_ids ||= ranked_interacted_users_ids.sort do |id1, id2|
      id2[1] <=> id1[1]
    end.map do |id|
      id[0]
    end
  end

  def ranked_interacted_users_ids
    @ranked_interacted_users_ids ||= interacted_users_ids.reduce({}) do |hash, id|
      hash[id] = hash[id].to_i + 1
      hash
    end
  end

  def events_recipient_ids
    @events_recipient_ids ||= events.map { |event| event.recipient_id }
  end

  def received_events_user_ids
    @received_events_user_ids ||= received_events.map { |event| event.user_id }
  end

  def interacted_users_ids
    @interacted_users_ids ||= events_recipient_ids + received_events_user_ids
  end
end
