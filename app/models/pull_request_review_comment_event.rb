class PullRequestReviewCommentEvent < Event
  def set_from_payload
    self.recipient = User.find_or_create_from_github(payload['pull_request']['user'])
    self.event_url = payload['comment']['html_url']
    self.action = payload['action']
  end
end
