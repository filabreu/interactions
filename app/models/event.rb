class Event < ApplicationRecord
  belongs_to :user
  belongs_to :recipient, class_name: 'User'

  before_validation :set_from_payload

  validates :source_id, presence: true
  validates :source_id, uniqueness: true
  validates :type, presence: true
  validates :user, presence: true
  validates :recipient, presence: true
  validates :recipient_id, numericality: {
    other_than: -> e { e.user_id },
    message: "can't be equal to user_id"
  }

  attr_accessor :payload

  def self.create_from_github(data)
    return if self.exists?(source_id: data['id'])

    user = User.find_or_create_from_github(data['actor'])

    user.events.create(
      source_id: data['id'],
      source_created_at: data['created_at'],
      type: data['type'],
      payload: data['payload']
    )
  end
end
