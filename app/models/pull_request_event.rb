class PullRequestEvent < Event
  def set_from_payload
    self.recipient = User.find_or_create_from_github(payload['pull_request']['user'])
    self.event_url = payload['pull_request']['html_url']
    self.action = if (
        payload['action'] == 'closed' &&
        payload['pull_request']['merged']
    )
      'merged'
    else
      payload['action']
    end
  end
end
