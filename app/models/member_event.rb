class MemberEvent < Event
  def set_from_payload
    self.recipient = User.find_or_create_from_github(payload['member'])
    self.event_url = payload['repository']['html_url']
    self.action = payload['action']
  end
end
