import React, { useState, useEffect } from "react";

export default function Home() {
  const [username, setUsername] = useState('');
  const [chosenUsername, setChosenUsername] = useState('');
  const [isPending, setIsPending] = useState(false);

  useEffect(() => {
    if (chosenUsername) {
      setIsPending(true);
      fetch(`/api/users/${username}`)
        .then(res => res.json())
        .then(data => {
          window.location.href = `/${data.username}`;
        });
    }
  }, [chosenUsername]);

  const handleSubmit = e => {
    e.preventDefault();
    setChosenUsername(username);
  }

  const handleInputChange = e => {
    setUsername(e.currentTarget.value);
  }

  return (
    <div className="w-100 mt5 flex justify-center">
      <div className="w-50 tc">
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="username">Choose an username</label>
          </div>
          <div className="mt2">
            <input
              id="username"
              type="text"
              name="username"
              className="br2 ba b--silver pa2 gray"
              value={username}
              onChange={handleInputChange}
            />
          </div>
          <button
            className={`mt2 bw0 br2 pv2 ph3
              ${isPending ? "bg-light-gray" : "bg-blue white pointer"}`}
            type="submit"
            disabled={isPending}
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
