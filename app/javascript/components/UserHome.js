import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Slider from "infinite-react-carousel";

export default function UserHome({ match }) {
  const { username } = match.params;
  const [users, setUsers] = useState(null);
  const [usersEvents, setUserEvents] = useState(null);

  useEffect(() => {
    if (!users) {
      fetch(`/api/users/${username}/interacted_users`)
        .then(res => res.json())
        .then(data => {
          setUsers(data.users);
          setUserEvents(data.users_events_counts);
        });
    }
  }, [users]);

  return (
    <div className="w-100 mt5 flex justify-center">
      <div className="w-70 tc">
        <Link
          to={`/${username}/interactions`}
          className="ph4 pv3 br3 bg-blue white no-underline-l"
        >
          See your Interactions stats
        </Link>
        {users && usersEvents && (
          <Slider className="mt4" slidesToShow={3} arrows={false} dots>
            {users.map(user => (
              <div key={user.id} className="ph2">
                <div className="pa4 br3 bg-near-white">
                  <img className="br3 w-50" src={user.avatar_url} />
                  <div className="mt2">{user.username}</div>
                  <div className="mt2 gray">
                    {usersEvents[user.id]} Recent Events
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        )}
      </div>
    </div>
  );
}
