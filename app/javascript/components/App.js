import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from "./Home";
import UserHome from "./UserHome";
import Interactions from "./Interactions";

export default function App() {
  return (
    <div className="mw-100 sans-serif">
      <Router>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/:username/interactions" exact={true} component={Interactions} />
          <Route path="/:username" component={UserHome} />
        </Switch>
      </Router>
    </div>
  );
}
