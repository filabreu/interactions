import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Interactions({ match }) {
  const { username } = match.params;
  const [interactions, setInteractions] = useState(null);
  const [dateFilter, setDateFilter] = useState("");
  const [dateSince, setDateSince] = useState("");
  const [orderBy, setOrderBy] = useState("count");
  const [orderAsc, setOrderAsc] = useState(false);
  const [isPending, setIsPending] = useState(false);

  useEffect(() => {
    if (!interactions) {
      const protocol = window.location.protocol;
      const host = window.location.host;
      const url = new URL(
        `${protocol}//${host}/api/users/${username}/interactions/ranked`
      );

      if (dateSince) {
        const params = { date_since: dateSince };
        Object.keys(params).forEach(key =>
          url.searchParams.append(key, params[key])
        );
      }

      setIsPending(true);

      fetch(url)
        .then(res => res.json())
        .then(data => {
          if (data.length !== 0) {
            setIsPending(false);
            setInteractions(data);
          }
        });
    }
  }, [interactions, dateSince]);

  const orderInteractions = () => {
    if (!interactions)
      return interactions;

    return interactions.sort((a, b) =>
      orderAsc ? a[orderBy] > b[orderBy] : a[orderBy] < b[orderBy]
    );
  };

  const handleFilterSubmit = (e) => {
    e.preventDefault();

    if (dateFilter) {
      setInteractions(null);
      setDateSince(dateFilter);
    }
  }

  const handleDateFilterChange = (e) => {
    setDateFilter(e.target.value);
  }

  const orderedInteractions = orderInteractions();

  return (
    <div className="w-100 mt5 flex justify-center">
      <div className="w-70 justify-center">
        <div>
          <Link
            to={`/${username}`}
            className="ph4 pv3 br3 bg-blue white no-underline-l"
          >
            &lsaquo; Back to Home
          </Link>
        </div>
        <form className="w-100 mt4" onSubmit={handleFilterSubmit}>
          <div className="flex">
            <input
              name="date_filter"
              type="date"
              className="mr4 br2 ba b--silver pa2 gray"
              value={dateFilter}
              onChange={handleDateFilterChange}
            />
            <div className="">
              <button
                className={`mt2 bw0 br2 pv2 ph3
              ${isPending ? "bg-light-gray" : "bg-blue white pointer"}`}
                type="submit"
                disabled={isPending}
              >
                Submit
              </button>
            </div>
          </div>
        </form>
        {orderedInteractions && (
          <table className="collapse ba br2 b--black-10 mt4 pv2 ph3">
            <tbody>
              <tr className="striped--light-gray">
                <th
                  className="pv2 ph3 tl f6 fw6 ttu pointer"
                  onClick={() => {
                    if (orderBy === "type") {
                      setOrderAsc(!orderAsc);
                    }
                    setOrderBy("type");
                  }}
                >
                  Type
                  {orderAsc && <>&uarr;</>}
                  {!orderAsc && <>&darr;</>}
                </th>
                <th
                  className="pv2 ph3 tl f6 fw6 ttu pointer"
                  onClick={() => {
                    if (orderBy === "count") {
                      setOrderAsc(!orderAsc);
                    }
                    setOrderBy("count");
                  }}
                >
                  Count
                  {orderAsc && <>&uarr;</>}
                  {!orderAsc && <>&darr;</>}
                </th>
              </tr>
              {orderedInteractions.map(interaction => (
                <tr className="striped--light-gray" key={interaction.type}>
                  <td className="pv2 ph3">{interaction.type}</td>
                  <td className="pv2 ph3">{interaction.count}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
}
