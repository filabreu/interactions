module Github
  class HTTPError < StandardError; end

  class Client
    def get_user_events(username)
      @username = username

      res = Net::HTTP.get_response(user_events_uri)

      raise Github::HTTPError unless res.code == '200'

      JSON.parse(res.body)
    end

    def get_user_received_events(username)
      @username = username

      res = Net::HTTP.get_response(user_received_events_uri)

      raise Github::HTTPError unless res.code == '200'

      JSON.parse(res.body)
    end

    def user_events_uri
      URI("#{base_url}/events")
    end

    def user_received_events_uri
      URI("#{base_url}/received_events")
    end

    def base_url
      "https://api.github.com/users/#{@username}"
    end
  end
end
