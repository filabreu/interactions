module Api
  class InteractionsController < ApplicationController
    def index
      interactions = current_user.interactions

      render json: interactions.as_json
    end

    def ranked
      ranked_interaction_types = current_user.ranked_interaction_types(date_since: params[:date_since])

      render json: ranked_interaction_types.as_json
    end

    private

    def current_user
      User.find_by(username: params[:user_username])
    end
  end
end
