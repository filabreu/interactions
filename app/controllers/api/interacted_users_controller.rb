module Api
  class InteractedUsersController < ApplicationController
    def index
      interacted_users = current_user.interacted_users
      ranked_interacted_users_ids = current_user.ranked_interacted_users_ids

      render json: { users: interacted_users.as_json, users_events_counts: ranked_interacted_users_ids }
    end

    private

    def current_user
      User.find_by(username: params[:user_username])
    end
  end
end
