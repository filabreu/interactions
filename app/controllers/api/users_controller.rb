module Api
  class UsersController < ApplicationController
    def show
      CreateUserEventsJob.new(username: params[:username]).perform_now

      render json: { username: params[:username] }
    end
  end
end
