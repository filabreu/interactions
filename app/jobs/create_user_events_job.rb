class CreateUserEventsJob < ApplicationJob
  queue_as :default

  def perform(username:)
    @username = username

    create_events(events)
    create_events(received_events)
  end

  def client
    @client ||= Github::Client.new
  end

  def events
    @events ||= client.get_user_events(@username)
  end

  def received_events
    @received_events ||= client.get_user_received_events(@username)
  end

  def create_events(events_data)
    events_data.each do |event_data|
      begin
        Event.create_from_github(event_data)
      rescue ActiveRecord::SubclassNotFound
        next
      end
    end
  end
end
