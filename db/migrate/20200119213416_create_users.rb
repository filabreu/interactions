class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.integer :source_id, null: false, index: { unique: true }
      t.string :username, index: true
      t.string :avatar_url
      t.string :profile_url

      t.timestamps
    end
  end
end
