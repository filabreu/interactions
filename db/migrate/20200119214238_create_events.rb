class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :recipient, null: false, foreign_key: { to_table: :users }
      t.string :type
      t.integer :source_id, null: false, index: { unique: true }
      t.datetime :source_created_at
      t.string :event_url
      t.string :action

      t.timestamps
    end
  end
end
