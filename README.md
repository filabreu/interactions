# INTERACTIONS | Test for Cleary by Filipe Abreu

* Ruby version
2.7.0

* System dependencies
Ruby on Rails 6.0.x
RubyGems
Bundler
Node
Yarn

* Configuration
Having Ruby 2.7.0, RubyGems and Bundler installed, run `bundle install` to install all gems dependencies.

Having Node and Yarn installed, run `yarn install` to install all node packages dependencies

* Database creation
Run `rake db:setup`

* Start application
After installing all dependencies and creating the database:

Start the backend application by running:

```
rails s
```

Start the frontend application by running:
```
./bin/webpack-dev-server
```

Finally, access the application on your browser at `http://localhost:3000`

