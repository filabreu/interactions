Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'home#index'

  namespace :api do
    resources :users, param: :username, only: [:show] do
      resources :interacted_users, only: :index
      resources :interactions, only: :index do
        get 'ranked', on: :collection
      end
    end
  end

  get '/:username/interactions', to: 'home#index'
  get '/:username', to: 'home#index'
end
